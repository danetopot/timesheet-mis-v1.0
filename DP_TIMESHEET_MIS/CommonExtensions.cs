﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DP_TIMESHEET_MIS
{
    public static class CommonExtensions
    {
        public static void SetObject(IHttpContextAccessor httpContext, string key, object value)
        {
            httpContext.HttpContext.Session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObject<T>(IHttpContextAccessor httpContext, string key)
        {
            var value = httpContext.HttpContext.Session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
