#pragma checksum "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2d3386492fd48789659101031bfa2e83dc8b69bf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Timesheet_ViewTimesheet), @"mvc.1.0.view", @"/Views/Timesheet/ViewTimesheet.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Timesheet/ViewTimesheet.cshtml", typeof(AspNetCore.Views_Timesheet_ViewTimesheet))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS;

#line default
#line hidden
#line 2 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\_ViewImports.cshtml"
using DP_TIMESHEET_MIS.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2d3386492fd48789659101031bfa2e83dc8b69bf", @"/Views/Timesheet/ViewTimesheet.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6e11c0368ce592f6bde09cf8da08ba065c773b19", @"/Views/_ViewImports.cshtml")]
    public class Views_Timesheet_ViewTimesheet : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DP_TIMESHEET_MIS.Models.ViewModels.Timesheet.ViewTimesheetViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("mb-xs mt-xs mr-xs btn btn-lg btn-default"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "SearchTimesheet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Timesheet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(68, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
  
    ViewData["Title"] = "View Timesheet";

#line default
#line hidden
            BeginContext(196, 2227, true);
            WriteLiteral(@"

<section role=""main"" class=""content-body"">
    <header class=""page-header"">
        <h2>Timesheet</h2>

        <div class=""right-wrapper pull-right"">
            <ol class=""breadcrumbs"">
                <li>
                    <a href=""index.html"">
                        <i class=""fa fa-home""></i>
                    </a>
                </li>
                <li><span>Timesheet</span></li>
                <li><span>View</span></li>
            </ol>

            <a class=""sidebar-right-toggle"" data-open=""sidebar-right""><i class=""fa fa-chevron-left""></i></a>
        </div>
    </header>
    <div class=""row"">
        <div class=""col-lg-12"">
            <section class=""panel"">
                <header class=""panel-heading"">
                    <div class=""panel-actions"">
                        <a href=""#"" class=""fa fa-caret-down""></a>
                        <a href=""#"" class=""fa fa-times""></a>
                    </div>

                    <h2 class=""panel-title"">View Timeshe");
            WriteLiteral(@"et</h2>
                    <p class=""panel-subtitle"">
                        Please view the open timesheet.
                    </p>
                </header>
                <div class=""panel-body"">
                    <div>
                        <div class=""alert alert-info"" style=""text-align:center"">
                            <strong>CONSULTANT NAME</strong>
                            <br />
                            October 2018 - November 2019
                        </div>
                    </div>

                    <div class=""table-responsive"" style=""height: 430px"">
                        <table class=""table table-striped table-bordered mb-none table-font"">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th><b>DATE WORKED</b></th>
                                    <th><b>HOURS WORKED</b></th>
                                    <th><b>PROJECT</b></th>
     ");
            WriteLiteral("                               <th><b>TASK DESCRIPTION</b></th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n");
            EndContext();
#line 61 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                  int i = 1;

#line default
#line hidden
            BeginContext(2470, 32, true);
            WriteLiteral("                                ");
            EndContext();
#line 62 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                 foreach (var item in Model.TimesheetDetail)
                                {

#line default
#line hidden
            BeginContext(2583, 138, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>\r\n                                            <span>");
            EndContext();
            BeginContext(2722, 1, false);
#line 66 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                             Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(2723, 147, true);
            WriteLiteral(".</span>\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2871, 15, false);
#line 69 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                       Write(item.DateFilled);

#line default
#line hidden
            EndContext();
            BeginContext(2886, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(3026, 16, false);
#line 72 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                       Write(item.HoursWorked);

#line default
#line hidden
            EndContext();
            BeginContext(3042, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(3182, 31, false);
#line 75 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                       Write(item.Project.ProjectDescription);

#line default
#line hidden
            EndContext();
            BeginContext(3213, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(3353, 20, false);
#line 78 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                       Write(item.TaskDescription);

#line default
#line hidden
            EndContext();
            BeginContext(3373, 92, true);
            WriteLiteral("\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 81 "C:\Users\Danet Opot\Desktop\PROJECTS\dp_timesheet_mis\DP_TIMESHEET_MIS\Views\Timesheet\ViewTimesheet.cshtml"
                                    i++;
                                }

#line default
#line hidden
            BeginContext(3542, 200, true);
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                    <hr />\r\n                    <div style=\"float: left;\">\r\n                        ");
            EndContext();
            BeginContext(3742, 157, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "fec3c82517ef4d19968d0b3c4df50f14", async() => {
                BeginContext(3850, 45, true);
                WriteLiteral(" Back &nbsp;<i class=\"fa fa-mail-reply \"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3899, 105, true);
            WriteLiteral("\r\n\r\n                    </div>\r\n                    <div style=\"float: right;\">\r\n                        ");
            EndContext();
            BeginContext(4004, 152, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5eca9b47e29241e4b92f532cc90c534e", async() => {
                BeginContext(4112, 40, true);
                WriteLiteral(" Print &nbsp;<i class=\"fa fa-print\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4156, 120, true);
            WriteLiteral("\r\n\r\n                    </div>\r\n                </div>\r\n            </section>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DP_TIMESHEET_MIS.Models.ViewModels.Timesheet.ViewTimesheetViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
