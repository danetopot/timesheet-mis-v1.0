﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS
{
    public class Common
    {
        private DatabaseContext dbContext;
        private IHttpContextAccessor httpContextAccessor;

        public Common(DatabaseContext context, IHttpContextAccessor httpContext)
        {
            dbContext = context;
            httpContextAccessor = httpContext;
        }

        public Object LoadCombo(int type)
        {
            if (type == 1)
            {
                var worktypes = dbContext.WorkType.ToList();
                return worktypes;
            }
            if (type == 2)
            {
                var periods = dbContext.Period.ToList();
                return periods;
            }
            if (type == 3)
            {
                var projects = dbContext.Projects.ToList();
                return projects;
            }
            if (type == 4)
            {
                var positions = dbContext.Position.ToList();
                return positions;
            }
            if (type == 5)
            {
                var roles = dbContext.UserSystemRoles.ToList();
                return roles;
            }
            return 0;
        }

        public string GetHash(string text)
        {
            using (var sha256 = SHA256.Create())
            {
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            };
        }

        public void SetSessionVariables(IHttpContextAccessor httpContext, UsersModel user)
        {
            if (user != null)
            {
                // Get Who
                int consultantid = user.Consultant.ConsultantId;
                var Consultant = dbContext.Consultants
                    //.Include(t => t.Consultant.ConsultantTypeName)
                    .FirstOrDefault(p => p.ConsultantId == consultantid);

                // Get Position
                var UserRoles = dbContext.UserRoles
                    .Include(u => u.Role)
                    .Where(u => u.User.UserId == user.UserId).ToList();

                var Username = user.UserName;
                var ConsultantName = Consultant.ConsultantName;
                List<UserRolesModel> ConsultantUserRoles = UserRoles;
                List<String> Roles = new List<String>();
                foreach (var UserRole in ConsultantUserRoles)
                {
                    var Role = UserRole.Role.Id;
                    Roles.Add(Role.ToString());
                }
                String UserRole__ = string.Join(",", Roles);

                httpContext.HttpContext.Session.SetInt32("Authenticated", 1);
                httpContext.HttpContext.Session.SetString("Username", Username);
                httpContext.HttpContext.Session.SetString("ConsultantName", ConsultantName);
                httpContext.HttpContext.Session.SetString("ConsultantRoles", UserRole__);
            }

        }

        public Dictionary<int, string> GetSessionVariables(IHttpContextAccessor httpContext)
        {
            var Authenticated = httpContext.HttpContext.Session.GetInt32("Authenticated");
            var Username = httpContext.HttpContext.Session.GetString("Username");
            var ConsultantName = httpContext.HttpContext.Session.GetString("ConsultantName");
            var ConsultantRoles = httpContext.HttpContext.Session.GetString("ConsultantRoles");

            Dictionary<int, string> SessionData = new Dictionary<int, string>()
            {
                { 1, Username},
                { 2, ConsultantName},
                { 3, ConsultantRoles},
                { 4, Authenticated.ToString()}
            };
            return SessionData;
        }

        
    }
}
