﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels;
using DP_TIMESHEET_MIS.Models.ViewModels.Account;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Authentication;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;

namespace DP_TIMESHEET_MIS.Controllers
{
    public class AccountController : Controller
    {
        private Common common;
        private RegisterViewModel regViewModel;
        private SearchUserViewModel searchUserViewModel;
        private ManageUserViewModel manageUserViewModel;
        private DatabaseContext dbContext;
        private HttpContext httpContext;
        private IHttpContextAccessor httpContextAccessor;

        public AccountController(DatabaseContext context, IHttpContextAccessor httpContext_Accessor)
        {
            dbContext = context;
            httpContextAccessor = httpContext_Accessor;
            common = new Common(dbContext, httpContextAccessor);
            regViewModel = new RegisterViewModel();
            searchUserViewModel = new SearchUserViewModel();
            manageUserViewModel = new ManageUserViewModel();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var RememberMe = model.RememberMe;
            if (ModelState.IsValid)
            {
                var user = dbContext.Users.Include("Consultant").Where(
                    u => u.UserName.Equals(model.UserName) &&
                    u.Password.Equals(common.GetHash(model.Password))
                    ).FirstOrDefault();
                if (user !=null)
                {
                    int consultantid = user.Consultant.ConsultantId;
                    var consultant = dbContext.Consultants.FirstOrDefault(
                    p => p.ConsultantId == consultantid
                    );

                    // ClaimsIdentity Stock Code
                    var identity = new ClaimsIdentity(
                        CookieAuthenticationDefaults.AuthenticationScheme, 
                        ClaimTypes.Name,
                        ClaimTypes.Role);
                    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, model.UserName));
                    identity.AddClaim(new Claim(ClaimTypes.Name, model.UserName));
                    identity.AddClaim(new Claim("Password", common.GetHash(model.Password)));

                    var principal = new ClaimsPrincipal(identity);
                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        principal,
                        new Microsoft.AspNetCore.Authentication.AuthenticationProperties {
                            IsPersistent = model.RememberMe
                        });

                    // common.SetSessionVariables(httpContextAccessor, model.UserName, common.GetHash(model.Password));
                    common.SetSessionVariables(httpContextAccessor, user);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("LoginError","Wrong Username or Password!");
                    return View(model);
                }
            }
            ModelState.AddModelError("LoginError", "Please provide the required details");
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Logout()
        {
            return RedirectToAction("Login");
        }

        [HttpGet]
        public IActionResult Register()
        {
            regViewModel.WorkTypeList = common.LoadCombo(1) as List<WorkTypeModel>;
            return View(regViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, IList<IFormFile> ConsultantSignature)
        {

            var signature = new byte[1000];
            foreach (var img in ConsultantSignature)
            {
                if (img.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await img.CopyToAsync(stream);
                        signature = stream.ToArray();
                    }
                }
            }

            ModelState.Remove("ConsultantSignature");
            if (ModelState.IsValid)
            {
                var Consultant = new ConsultantModel
                {
                    ConsultantName = model.Fullname,
                    Email = model.Email,
                    ConsultantSignature = signature,
                    ContractStartDate = model.ContractStartDate ?? DateTime.Now,
                    ContractEndDate = model.ContractEndDate ?? DateTime.Now
                };
                dbContext.Consultants.Add(Consultant);
                dbContext.SaveChanges();
                int Pk = Consultant.ConsultantId;
                return RedirectToAction("NewUserAccount", new { PriKey = Pk });
            }
            ModelState.AddModelError("RegisterError", "Please provide the required details");
            model.WorkTypeList = common.LoadCombo(1) as List<WorkTypeModel>;
            return View(model);
        }

        [HttpGet]
        public IActionResult NewUserAccount(int? PriKey)
        {
            int Pk = (int)PriKey;
            TempData["PrimaryKey"] = Pk;
            return View();
        }

        [HttpPost]
        public IActionResult NewUserAccount(NewUserAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                ConsultantModel co = new ConsultantModel() { ConsultantId = (int)model.Consultant }; dbContext.Attach(co);

                var User = new UsersModel
                {
                    UserName = model.UserName,
                    Password = common.GetHash(model.Password),
                    Consultant = co
                };
                
                //DateTime today = DateTime.Today;
                dbContext.Users.Add(User);
                dbContext.SaveChanges();

                // Grant Guest Roles
                int Pk = User.UserId;
                UsersModel usermodel = new UsersModel() { UserId = (int)Pk }; 
                UserSystemRoles usersysrolemodel = new UserSystemRoles() { Id = 2 }; dbContext.Attach(usersysrolemodel);
                var UserRoles = new UserRolesModel
                {
                    User = usermodel,
                    Role = usersysrolemodel
                };
                dbContext.UserRoles.Add(UserRoles);
                dbContext.SaveChanges();

                return RedirectToAction("Login");
            }
            ModelState.AddModelError("NewUserAccountError", "Please provide the required details");
            return View(model);
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ManageRole(int?id, ManageUserViewModel model)
        {
            List<string> Roles = new List<string>();
            var UserId = id;
            var UserRoles = dbContext.UserRoles.Include(p=>p.Role)
                .Where(
                    u => u.User.UserId == id
                    ).ToList();

            foreach(var userRole in UserRoles)
            {
                string rolename = userRole.Role.RoleName;
                Roles.Add(rolename);
            }

            // Get Session Data
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;

            model.UserSystemRolesList = common.LoadCombo(5) as List<UserSystemRoles>;
            model.RoleNames = Roles;
            model.UserID =(int)UserId;
            ModelState.Remove("Roles");
            return View(model);
        }

        [HttpPost]
        public IActionResult ManageRole(ManageUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var UserID = model.UserID;
                int?[] roles = model.Roles;
                foreach(var role in roles)
                {
                    UsersModel usermodel = new UsersModel() { UserId = (int)UserID }; dbContext.Attach(usermodel);
                    UserSystemRoles usersysrolemodel = new UserSystemRoles() { Id = (int)role }; dbContext.Attach(usersysrolemodel);
                    var UserRoles = new UserRolesModel
                    {
                        User = usermodel,
                        Role = usersysrolemodel

                    };
                    dbContext.UserRoles.Add(UserRoles);
                    dbContext.SaveChanges();
                }

                TempData["message"] = "User Roles Added Successfully";
                return RedirectToAction("SearchUser");
            }
            ModelState.AddModelError("ManageRoleError", "Please provide the required details");
            model.UserSystemRolesList = common.LoadCombo(5) as List<UserSystemRoles>;
            return View(model);
        }

        [HttpGet]
        public IActionResult SearchUser()
        {
            // Get Session Data
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            searchUserViewModel.SessionData = SessionData;
            return View(searchUserViewModel);
        }


        [HttpPost]
        public IActionResult SearchUser(SearchUserViewModel model)
        {
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;
            if (ModelState.IsValid)
            {
                var User = dbContext.Users
                    .Include(c => c.Consultant)
                    .Where(t => t.Consultant.Email.Equals(model.Email));

                model.Users = User;
                return View(model);
            }
            return View(model);
        }
    }
}