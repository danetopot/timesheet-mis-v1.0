﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;
using Microsoft.AspNetCore.Http;
using DP_TIMESHEET_MIS.Models;

namespace DP_TIMESHEET_MIS.Controllers
{
    public class ReportController : Controller
    {
        private Common common;
        private DatabaseContext dbContext;
        private IHttpContextAccessor httpContextAccessor;

        public ReportController(DatabaseContext context, IHttpContextAccessor httpContext_Accessor)
        {
            dbContext = context;
            httpContextAccessor = httpContext_Accessor;
            common = new Common(dbContext, httpContextAccessor);
        }

        [HttpGet]
        public IActionResult GenerateReport(MainViewModel model)
        {
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;
            return View(model);
        }
    }
}