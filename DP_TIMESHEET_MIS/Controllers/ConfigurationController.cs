﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels.Administration;

namespace DP_TIMESHEET_MIS.Controllers
{
    public class ConfigurationController : Controller
    {
        private Common common;
        private DatabaseContext dbContext;
        private HttpContext httpContext;
        private IHttpContextAccessor httpContextAccessor;
        private AdministrationViewModel adminViewModel;


        public ConfigurationController(DatabaseContext context, IHttpContextAccessor httpContext_Accessor)
        {
            dbContext = context;
            httpContextAccessor = httpContext_Accessor;
            common = new Common(dbContext, httpContextAccessor);
            adminViewModel = new AdministrationViewModel();

        }

        [HttpGet]
        public IActionResult SystemSettings()
        {
            // Get Session Data
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            adminViewModel.SessionData = SessionData;
            return View(adminViewModel);
        }
    }
}