﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels;
using DP_TIMESHEET_MIS.Models.ViewModels.Timesheet;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace DP_TIMESHEET_MIS.Controllers
{
    public class TimesheetController : Controller
    {
        private Common common;
        private DatabaseContext dbContext;
        private IHttpContextAccessor httpContextAccessor;
        private NewTimesheetViewModel tsViewModel;
        private ViewTimesheetViewModel vtsViewModel;
        private SearchTimesheetViewModel searchTsViewModel;


        public TimesheetController(DatabaseContext context, IHttpContextAccessor httpContext_Accessor)
        {
            dbContext = context;
            httpContextAccessor = httpContext_Accessor;
            common = new Common(dbContext, httpContextAccessor);
            tsViewModel = new NewTimesheetViewModel();
            vtsViewModel = new ViewTimesheetViewModel();
            searchTsViewModel = new SearchTimesheetViewModel();
        }

        [HttpGet]
        public IActionResult NewTimesheet()
        {
            // Get Session Data
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            tsViewModel.PeriodList = common.LoadCombo(2) as List<PeriodModel>;
            tsViewModel.ProjectList = common.LoadCombo(3) as List<ProjectModel>;
            tsViewModel.PositionList = common.LoadCombo(4) as List<PositionModel>;
            tsViewModel.SessionData = SessionData;


            return View(tsViewModel);
        }

        [HttpPost]
        public IActionResult NewTimesheet(NewTimesheetViewModel model)
        {
            if (ModelState.IsValid)
            {

                //Get UserIdentity
                var UserName = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var User = dbContext.Users.Include("Consultant").Where(
                    u => u.UserName.Equals(UserName)
                    ).FirstOrDefault();


                var consultant = User.Consultant.ConsultantId;
                var period = model.Period;
                var project = model.Project;
                var datefilled = model.DateFilled;
                var position = model.Position;
                var hoursworked = model.HoursWorked;
                var location = model.Location;
                var taskdescription = model.TaskDescription;
                var timesheetstatus = "Incomplete";

                // Validate Periods
                var periodModel = dbContext.Period.Find(period);
                DateTime PeriodStart = Convert.ToDateTime(periodModel.PeriodStartDate);
                DateTime PeriodStop = Convert.ToDateTime(periodModel.PeriodStopDate);
                int timesheetDuration = (int)(PeriodStop - PeriodStart).TotalDays;

                // Query # Of Timesheets Filled
                int ts_filled = dbContext.TimeSheetDetail.Where(
                    t => t.Header.Period.PeriodId == period).Count();
                if (timesheetDuration == ts_filled)
                {
                    TempData["TimesheetCompleteMsg"] = "You have already filled your timesheet for that period";
                    return RedirectToAction("SearchTimesheet");
                }
                if ((timesheetDuration - ts_filled) == 1)
                {
                    timesheetstatus = "Pending";
                }

                // Query for existing Timesheet For The Period
                var ts_header = dbContext.TimeSheetHeader.FirstOrDefault(
                    t => t.Consultant.ConsultantId == consultant &&
                    t.Period.PeriodId == period &&
                    t.IsActive == true);

                ConsultantModel co = new ConsultantModel() { ConsultantId = (int)consultant };
                ProjectModel pj = new ProjectModel() { ProjectId = (int)project }; dbContext.Attach(pj);
                PositionModel po = new PositionModel() { PositionId = (int)position }; dbContext.Attach(po);
                PeriodModel pd = new PeriodModel() { PeriodId = (int)period };

                var TimesheetHeader = new TimeSheetHeaderModel
                {
                    Consultant = co,
                    Period = pd,
                    TimesheetStatus = timesheetstatus,
                    ApprovedBy = null,
                    RejectedBy = null,
                    DateRejected = null,
                    IsActive = true
                };
                var TimesheetDetail = new TimeSheetDetailModel
                {
                    DateFilled = datefilled,
                    HoursWorked = hoursworked,
                    Location = location,
                    Position = po,
                    Project = pj,
                    TaskDescription = taskdescription
                };


                if (ts_header != null)
                {
                    TimeSheetHeaderModel th = new TimeSheetHeaderModel() { HeaderId = (int)ts_header.HeaderId };
                    TimesheetDetail.Header = th;
                    dbContext.TimeSheetDetail.Add(TimesheetDetail);
                    dbContext.SaveChanges();

                    TempData["SaveTimesheetMsg"] = "Timesheet Saved Successfully";
                    return RedirectToAction("SearchTimesheet");

                }
                else
                {
                    dbContext.TimeSheetHeader.Add(TimesheetHeader);
                    dbContext.SaveChanges();
                    var Pk = TimesheetHeader.HeaderId;

                    TimeSheetHeaderModel th = new TimeSheetHeaderModel() { HeaderId = (int)Pk };
                    TimesheetDetail.Header = th;
                    dbContext.TimeSheetDetail.Add(TimesheetDetail);
                    dbContext.SaveChanges();

                    TempData["SaveTimesheetMsg"] = "Timesheet Saved Successfully";
                    return RedirectToAction("SearchTimesheet");
                }


            }
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            ModelState.AddModelError("TimesheetError", "Please provide the required details");
            model.PeriodList = common.LoadCombo(2) as List<PeriodModel>;
            model.ProjectList = common.LoadCombo(3) as List<ProjectModel>;
            model.PositionList = common.LoadCombo(4) as List<PositionModel>;
            model.SessionData = SessionData;
            return View(model);
        }

        [HttpGet]
        public IActionResult ViewTimesheet(int? id, ViewTimesheetViewModel model)
        {
            var Timesheet = dbContext.TimeSheetDetail
                .Include(p => p.Project)
                .Where(t => t.Header.HeaderId.Equals(id));

            // Get Session Data
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;
            model.TimesheetDetail = Timesheet;
            model.HeaderID = (int)id;
            return View(model);
        }

        [HttpGet]
        public IActionResult EditTimesheet(int? id, EditTimesheetViewModel model, int? ctrl)
        {
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.PeriodList = common.LoadCombo(2) as List<PeriodModel>;
            model.ProjectList = common.LoadCombo(3) as List<ProjectModel>;
            model.PositionList = common.LoadCombo(4) as List<PositionModel>;
            model.SessionData = SessionData;

            // Get Data
            var Timesheets = dbContext.TimeSheetDetail
                // .Include("TimeSheetHeader")
                .Include(t => t.Header)
                .Where(p => p.Id == id);

            foreach (var Timesheet in Timesheets)
            {
                model.DetailId = Timesheet.Id;
                model.DateFilled = Timesheet.DateFilled;
                model.HoursWorked = Timesheet.HoursWorked;
                model.Location = Timesheet.Location;
                model.Period = Timesheet.Header.Period.PeriodId;
                model.Position = Timesheet.Position.PositionId;
                model.Project = Timesheet.Project.ProjectId;
                model.TaskDescription = Timesheet.TaskDescription;
            }
            ModelState.Clear();
            return View(model);
        }

        [HttpPost]
        public IActionResult EditTimesheet(int? id, EditTimesheetViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var TimesheetDetail = dbContext.TimeSheetDetail.Find(id);
                    var position = dbContext.Position.FirstOrDefault(x => x.PositionId == model.Position);
                    var project = dbContext.Projects.FirstOrDefault(x => x.ProjectId == model.Project);
                    var period = dbContext.Period.FirstOrDefault(x => x.PeriodId == model.Period);

                    TimesheetDetail.DateFilled = model.DateFilled;
                    TimesheetDetail.HoursWorked = model.HoursWorked;
                    TimesheetDetail.Location = model.Location;
                    TimesheetDetail.TaskDescription = model.TaskDescription;
                    TimesheetDetail.Position = position;
                    TimesheetDetail.Project = project;
                    TimesheetDetail.Header.Period = period;
                    dbContext.Entry(TimesheetDetail).CurrentValues.SetValues(model);
                    dbContext.SaveChanges();

                    TempData["SaveTimesheetMsg"] = "Timesheet Updated Successfully";
                    return RedirectToAction("SearchTimesheet");
                }
                catch (Exception ex)
                {
                    TempData["SaveTimesheetMsg"] = "Error Updating Timesheet :- " + ex.ToString();
                    return RedirectToAction("SearchTimesheet");
                }
            }
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            ModelState.AddModelError("TimesheetError", "Please provide the required details");
            model.PeriodList = common.LoadCombo(2) as List<PeriodModel>;
            model.ProjectList = common.LoadCombo(3) as List<ProjectModel>;
            model.PositionList = common.LoadCombo(4) as List<PositionModel>;
            model.SessionData = SessionData;
            return View(model);
        }

        [HttpGet]
        public IActionResult DeleteTimesheet(int? id, SearchTimesheetViewModel model)
        {
            var TimesheetHeader = dbContext.TimeSheetHeader.Find(id);
            dbContext.TimeSheetHeader.Remove(TimesheetHeader);
            dbContext.SaveChanges();

            TempData["SaveTimesheetMsg"] = "Timesheet Deleted Successfully";
            return RedirectToAction("SearchTimesheet");
        }

        [HttpGet]
        public IActionResult DeleteTimesheetDetail(int? id, EditTimesheetViewModel model)
        {
            var TimesheetDetail = dbContext.TimeSheetDetail.Find(id);
            dbContext.TimeSheetDetail.Remove(TimesheetDetail);
            dbContext.SaveChanges();

            TempData["SaveTimesheetMsg"] = "Timesheet Details Deleted Successfully";
            return RedirectToAction("SearchTimesheet");
        }

        [HttpGet]
        public IActionResult SearchTimesheetDetails(int? id, SearchTimesheetDetailViewModel model)
        {
            var Timesheets = dbContext.TimeSheetDetail
                .Where(p => p.Header.HeaderId == id);

            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;
            model.TimeSheetDetail = Timesheets;
            return View(model);
        }

        [HttpGet]
        public IActionResult SearchTimesheet(int? param, SearchTimesheetViewModel model)
        {
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.PeriodList = common.LoadCombo(2) as List<PeriodModel>;
            model.SessionData = SessionData;

            return View(model);
        }

        [HttpPost]
        public IActionResult SearchTimesheet(SearchTimesheetViewModel model)
        {
            string ConsultantName = model.ConsultantName;
            int Period = Convert.ToInt32(model.Period);
            var Timesheets = dbContext.TimeSheetHeader
                .Include(c => c.Consultant)
                .Include(p => p.Period);
            List<TimeSheetHeaderModel> QuerySet = new List<TimeSheetHeaderModel>();

            if (ConsultantName != null && Period == 0)
            {
                QuerySet = Timesheets.Where(
                    t => t.Consultant.ConsultantName.Contains(ConsultantName)
                    ).ToList<TimeSheetHeaderModel>();
            }

            if (Period > 0 && ConsultantName == null)
            {
                QuerySet = Timesheets.Where(
                    t => t.Period.PeriodId.Equals(Period)
                    ).ToList<TimeSheetHeaderModel>();
            }

            if (ConsultantName != null && Period > 0)
            {
                QuerySet = Timesheets.Where(
                  t => t.Consultant.ConsultantName.Contains(ConsultantName) &&
                  t.Period.PeriodId.Equals(Period)
                  ).ToList<TimeSheetHeaderModel>();
            }

            model.TimeSheetHeader = QuerySet;
            model.PeriodList = common.LoadCombo(2) as List<PeriodModel>;
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;

            return View(model);
        }

        [HttpGet]
        public IActionResult ApproveTimesheet(int? id, ApproveTimesheetViewModel model)
        {
            // Get Session Data
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            var Timesheet = dbContext.TimeSheetDetail
                .Include(p => p.Project)
                .Where(t => t.Header.HeaderId.Equals(id));
            model.SessionData = SessionData;
            model.TimesheetDetail = Timesheet;
            model.HeaderID = (int)id;
            return View(model);
        }

        [HttpPost]
        public IActionResult ApproveTimesheet(ApproveTimesheetViewModel model)
        {
            var Timesheet = dbContext.TimeSheetHeader.SingleOrDefault(
                b => b.HeaderId == model.HeaderID
                );
            Timesheet.TimesheetStatus = "Approved";
            dbContext.SaveChanges();
            return RedirectToAction("SearchTimesheet");
        }

        [HttpGet]
        public IActionResult RejectTimesheet(int? id)
        {
            var Timesheet = dbContext.TimeSheetHeader.SingleOrDefault(
                b => b.HeaderId == id
                );
            Timesheet.TimesheetStatus = "Rejected";
            dbContext.SaveChanges();
            return RedirectToAction("SearchTimesheet");
        }
    }
}