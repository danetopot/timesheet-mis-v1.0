﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;

namespace DP_TIMESHEET_MIS.Controllers
{
    public class HomeController : Controller
    {
        private Common common;
        private DatabaseContext dbContext;
        private IHttpContextAccessor httpContextAccessor;

        public HomeController(DatabaseContext context, IHttpContextAccessor httpContext_Accessor)
        {
            dbContext = context;
            httpContextAccessor= httpContext_Accessor;
            common = new Common(dbContext, httpContextAccessor);
        }

        [HttpGet]
        public IActionResult Index(MainViewModel model)
        {
            var SessionData = common.GetSessionVariables(httpContextAccessor);
            model.SessionData = SessionData;
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
