﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    [Table("TimeSheetHeader")]
    public class TimeSheetHeaderModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HeaderId { get; set; }

        [Required]
        public virtual ConsultantModel Consultant { get; set; }

        [Required]
        public virtual PeriodModel Period { get; set; }

        [Required]
        public string TimesheetStatus { get; set; }

        public int? ApprovedBy { get; set; }

        public DateTime? DateApproved { get; set; }

        public int? RejectedBy { get; set; }

        public DateTime? DateRejected { get; set; }

        [Required]
        public Boolean IsActive { get; set; }
    }
}
