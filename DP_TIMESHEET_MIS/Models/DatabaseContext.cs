﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<CountryModel> Countries { get; set; }
        public DbSet<UsersModel> Users { get; set; }
        public DbSet<UserRolesModel> UserRoles { get; set; }
        public DbSet<UserSystemRoles> UserSystemRoles { get; set; }
        public DbSet<ProjectModel> Projects { get; set; }
        public DbSet<ConsultantModel> Consultants { get; set; }
        public DbSet<ConsultantTypeModel> ConsultantType { get; set; }
        public DbSet<WorkTypeModel> WorkType { get; set; }
        public DbSet<PeriodModel> Period { get; set; }
        public DbSet<PositionModel> Position { get; set; }
        public DbSet<TimeSheetHeaderModel> TimeSheetHeader { get; set; }
        public DbSet<TimeSheetDetailModel> TimeSheetDetail { get; set; }
    }
}
