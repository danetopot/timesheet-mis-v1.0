﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    [Table("Users")]
    public class UsersModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        [StringLength(20)]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        public int UpdatedBy { get; set; }

        [Required]
        public DateTime LastLogin { get; set; }

        [Required]
        public Boolean IsActive { get; set; }

        [Required]
        public virtual ConsultantModel Consultant { get; set; }
    }
}
