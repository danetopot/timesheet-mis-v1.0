﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    [Table("Position")]
    public class PositionModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PositionId { get; set; }

        [Required]
        [StringLength(100)]
        public string PositionName { get; set; }
    }
}
