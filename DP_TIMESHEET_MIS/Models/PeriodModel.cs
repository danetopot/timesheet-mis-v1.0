﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    public class PeriodModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PeriodId { get; set; }

        public DateTime? PeriodStartDate { get; set; }

        public DateTime? PeriodStopDate { get; set; }

        public string PeriodDescription { get; set; }
    }
}
