﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    [Table("TimeSheetDetail")]
    public class TimeSheetDetailModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public virtual TimeSheetHeaderModel Header { get; set; }

        [Required]
        public DateTime DateFilled { get; set; }

        [Required]
        public int HoursWorked { get; set; }

        // [Required]
        // public virtual WorkTypeModel WorkType { get; set; }

        [Required]
        public virtual PositionModel Position { get; set; }

        [Required]
        public virtual ProjectModel Project { get; set; }

        [Required]
        [StringLength(100)]
        public string Location { get; set; }

        public string TaskDescription { get; set; }
    }
}
