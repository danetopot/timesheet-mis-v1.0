﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;


namespace DP_TIMESHEET_MIS.Models.ViewModels.Timesheet
{
    public class EditTimesheetViewModel : MainViewModel
    {
        public int? DetailId { get; set; }

        [Required]
        [Display(Name = "Period")]
        public int? Period { get; set; }

        [Required]
        [Display(Name = "Position")]
        public int? Position { get; set; }

        [Required]
        [Display(Name = "Project")]
        public int? Project { get; set; }

        [Required]
        public DateTime DateFilled { get; set; }

        [Required]
        public int HoursWorked { get; set; }

        [Required]
        public string Location { get; set; }

        public string TaskDescription { get; set; }

        public List<PositionModel> PositionList { get; set; }

        public List<PeriodModel> PeriodList { get; set; }

        public List<ProjectModel> ProjectList { get; set; }

        public IEnumerable<TimeSheetDetailModel> TimeSheetDetail { get; set; }

    }
}
