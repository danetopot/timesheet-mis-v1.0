﻿using DP_TIMESHEET_MIS.Models.ViewModels.Home;
using System.Collections.Generic;

namespace DP_TIMESHEET_MIS.Models.ViewModels.Timesheet
{
    public class SearchTimesheetViewModel : MainViewModel
    {
        public string ConsultantName { get; set; }
        public string TimesheetStatus { get; set; }
        public string Period { get; set; }
        public string SearchName { get; set; }

        public IEnumerable<TimeSheetHeaderModel> TimeSheetHeader { get; set; }

        public List<PeriodModel> PeriodList { get; set; }

    }
}
