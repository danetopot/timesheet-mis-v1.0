﻿using DP_TIMESHEET_MIS.Models.ViewModels.Home;
using System.Collections.Generic;

namespace DP_TIMESHEET_MIS.Models.ViewModels.Timesheet
{
    public class SearchTimesheetDetailViewModel : MainViewModel
    {
        public int? Period { get; set; }

        public IEnumerable<TimeSheetDetailModel> TimeSheetDetail { get; set; }
    }
}
