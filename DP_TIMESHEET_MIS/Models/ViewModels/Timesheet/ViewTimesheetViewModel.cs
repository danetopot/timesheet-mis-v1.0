﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;


namespace DP_TIMESHEET_MIS.Models.ViewModels.Timesheet
{
    public class ViewTimesheetViewModel : MainViewModel
    {
        public int HeaderID { get; set; }

        public IEnumerable<TimeSheetDetailModel> TimesheetDetail { get; set; }
    }
}
