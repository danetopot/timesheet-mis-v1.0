﻿using System.Collections.Generic;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;


namespace DP_TIMESHEET_MIS.Models.ViewModels.Timesheet
{
    public class DeleteTimesheetViewModel : MainViewModel
    {
        public int DetailID { get; set; }
    }
}
