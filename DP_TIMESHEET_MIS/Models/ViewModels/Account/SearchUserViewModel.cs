﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;

namespace DP_TIMESHEET_MIS.Models.ViewModels.Account
{
    public class SearchUserViewModel : MainViewModel
    {
        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public IEnumerable<UsersModel> Users { get; set; }
    }
}
