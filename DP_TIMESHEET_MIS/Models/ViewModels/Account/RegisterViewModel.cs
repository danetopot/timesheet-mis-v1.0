﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DP_TIMESHEET_MIS.Models;
using Microsoft.AspNetCore.Http;

namespace DP_TIMESHEET_MIS.Models.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Other Names")]
        public string OtherNames { get; set; }

        public string Fullname { get { return string.Format("{0} {1}", FirstName, OtherNames); } }

        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Consultant Type")]
        public int? ConsultantType { get; set; }

        [Required]
        [Display(Name = "Contract Start Date")]
        public DateTime? ContractStartDate { get; set; }

        [Required]
        [Display(Name = "Contract End Date")]
        public DateTime? ContractEndDate { get; set; }

        [Required]
        [Display(Name = "Signature")]
        public byte[] ConsultantSignature { get; set; }

        public IFormFile UploadedImage { get; set; }

        public List<WorkTypeModel> WorkTypeList { get; set; }

    }
}
