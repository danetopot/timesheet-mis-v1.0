﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DP_TIMESHEET_MIS.Models;
using DP_TIMESHEET_MIS.Models.ViewModels.Home;
using Microsoft.AspNetCore.Http;

namespace DP_TIMESHEET_MIS.Models.ViewModels.Account
{
    public class ManageUserViewModel : MainViewModel
    {
        [Required]
        public int?[] Roles { get; set; }

        public int UserID { get; set; }

        public List<string> RoleNames { get; set; }


        public List<UserSystemRoles> UserSystemRolesList { get; set; }

        // public MainViewModel MainViewModel { get; set; }
    }
}
