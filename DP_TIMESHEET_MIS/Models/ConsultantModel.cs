﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    [Table("Consultant")]
    public class ConsultantModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConsultantId { get; set; }

        [Required]
        [StringLength(100)]
        public string ConsultantName { get; set; }

        [Required]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public DateTime ContractStartDate { get; set; }

        [Required]
        public DateTime ContractEndDate { get; set; }

        [Required]
        public byte[] ConsultantSignature { get; set; }

        [Required]
        public Boolean IsActive { get; set; }

        public virtual ConsultantTypeModel Consultant { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        public int UpdatedBy { get; set; }
    }
}
