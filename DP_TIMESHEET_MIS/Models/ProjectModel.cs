﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DP_TIMESHEET_MIS.Models
{
    [Table("Projects")]
    public class ProjectModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectId { get; set; }

        [Required]
        [StringLength(100)]
        public string ProjectRefNo { get; set; }

        [Required]
        [StringLength(100)]
        public string ProjectName { get; set; }

        [Required]
        [StringLength(100)]
        public string ProjectAddress { get; set; }

        public string ProjectDescription { get; set; }

        public virtual ConsultantModel Supervisor { get; set; }

        public virtual PositionModel SupervisorRole { get; set; }

        public virtual CountryModel Country { get; set; }

        /*
        public static implicit operator ProjectModel(int? v)
        {
            throw new NotImplementedException();
        }*/
    }
}
