﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class ChangeFkName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserUserSystemRolesRoleId",
                table: "UserRoles");

            migrationBuilder.RenameColumn(
                name: "UserUserSystemRolesRoleId",
                table: "UserRoles",
                newName: "UserSystemRoleId");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_UserUserSystemRolesRoleId",
                table: "UserRoles",
                newName: "IX_UserRoles_UserSystemRoleId");

            migrationBuilder.CreateTable(
                name: "UsersGroup",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 20, nullable: false),
                    Password = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    LastLogin = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ConsultantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersGroup", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_UsersGroup_Consultant_ConsultantId",
                        column: x => x.ConsultantId,
                        principalTable: "Consultant",
                        principalColumn: "ConsultantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsersGroup_ConsultantId",
                table: "UsersGroup",
                column: "ConsultantId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemRoleId",
                table: "UserRoles",
                column: "UserSystemRoleId",
                principalTable: "UserSystemRoles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemRoleId",
                table: "UserRoles");

            migrationBuilder.DropTable(
                name: "UsersGroup");

            migrationBuilder.RenameColumn(
                name: "UserSystemRoleId",
                table: "UserRoles",
                newName: "UserUserSystemRolesRoleId");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_UserSystemRoleId",
                table: "UserRoles",
                newName: "IX_UserRoles_UserUserSystemRolesRoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserUserSystemRolesRoleId",
                table: "UserRoles",
                column: "UserUserSystemRolesRoleId",
                principalTable: "UserSystemRoles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
