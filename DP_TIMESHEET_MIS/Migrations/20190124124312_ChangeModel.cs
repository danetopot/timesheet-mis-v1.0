﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class ChangeModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsersGroup_Consultant_ConsultantId",
                table: "UsersGroup");

            migrationBuilder.DropIndex(
                name: "IX_UsersGroup_ConsultantId",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "ConsultantId",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "DateUpdated",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "LastLogin",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "UsersGroup");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "UsersGroup");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConsultantId",
                table: "UsersGroup",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "UsersGroup",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "UsersGroup",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdated",
                table: "UsersGroup",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastLogin",
                table: "UsersGroup",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "UsersGroup",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "UsersGroup",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "UsersGroup",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_UsersGroup_ConsultantId",
                table: "UsersGroup",
                column: "ConsultantId");

            migrationBuilder.AddForeignKey(
                name: "FK_UsersGroup_Consultant_ConsultantId",
                table: "UsersGroup",
                column: "ConsultantId",
                principalTable: "Consultant",
                principalColumn: "ConsultantId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
