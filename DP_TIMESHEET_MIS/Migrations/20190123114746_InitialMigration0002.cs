﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class InitialMigration0002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TimeSheetHeader_Projects_ProjectId",
                table: "TimeSheetHeader");

            migrationBuilder.DropIndex(
                name: "IX_TimeSheetHeader_ProjectId",
                table: "TimeSheetHeader");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "TimeSheetHeader");

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "TimeSheetDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheetDetail_ProjectId",
                table: "TimeSheetDetail",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSheetDetail_Projects_ProjectId",
                table: "TimeSheetDetail",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TimeSheetDetail_Projects_ProjectId",
                table: "TimeSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_TimeSheetDetail_ProjectId",
                table: "TimeSheetDetail");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "TimeSheetDetail");

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "TimeSheetHeader",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheetHeader_ProjectId",
                table: "TimeSheetHeader",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSheetHeader_Projects_ProjectId",
                table: "TimeSheetHeader",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
