﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class ChangeUserRolesModel___ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemRoleId",
                table: "UserRoles");

            migrationBuilder.RenameColumn(
                name: "RoleId",
                table: "UserSystemRoles",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "UserSystemRoleId",
                table: "UserRoles",
                newName: "UserSystemId");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_UserSystemRoleId",
                table: "UserRoles",
                newName: "IX_UserRoles_UserSystemId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemId",
                table: "UserRoles",
                column: "UserSystemId",
                principalTable: "UserSystemRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemId",
                table: "UserRoles");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "UserSystemRoles",
                newName: "RoleId");

            migrationBuilder.RenameColumn(
                name: "UserSystemId",
                table: "UserRoles",
                newName: "UserSystemRoleId");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_UserSystemId",
                table: "UserRoles",
                newName: "IX_UserRoles_UserSystemRoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemRoleId",
                table: "UserRoles",
                column: "UserSystemRoleId",
                principalTable: "UserSystemRoles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
