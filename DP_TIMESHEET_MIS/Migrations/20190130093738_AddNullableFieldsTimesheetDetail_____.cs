﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class AddNullableFieldsTimesheetDetail_____ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DetailId",
                table: "TimeSheetDetail",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "TimeSheetDetail",
                newName: "DetailId");
        }
    }
}
