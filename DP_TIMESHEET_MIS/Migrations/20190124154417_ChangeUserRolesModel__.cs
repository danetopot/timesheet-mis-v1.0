﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class ChangeUserRolesModel__ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserRoleId",
                table: "UserRoles",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "UserRoles",
                newName: "UserRoleId");
        }
    }
}
