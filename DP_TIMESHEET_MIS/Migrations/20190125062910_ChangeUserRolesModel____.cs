﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DP_TIMESHEET_MIS.Migrations
{
    public partial class ChangeUserRolesModel____ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemId",
                table: "UserRoles");

            migrationBuilder.RenameColumn(
                name: "UserSystemId",
                table: "UserRoles",
                newName: "RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_UserSystemId",
                table: "UserRoles",
                newName: "IX_UserRoles_RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_UserSystemRoles_RoleId",
                table: "UserRoles",
                column: "RoleId",
                principalTable: "UserSystemRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_UserSystemRoles_RoleId",
                table: "UserRoles");

            migrationBuilder.RenameColumn(
                name: "RoleId",
                table: "UserRoles",
                newName: "UserSystemId");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                newName: "IX_UserRoles_UserSystemId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_UserSystemRoles_UserSystemId",
                table: "UserRoles",
                column: "UserSystemId",
                principalTable: "UserSystemRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
